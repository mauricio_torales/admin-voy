<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Eventos Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Evento get($primaryKey, $options = [])
 * @method \App\Model\Entity\Evento newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Evento[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Evento|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Evento|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Evento patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Evento[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Evento findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class EventosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('eventos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->addBehavior('Josegonzalez/Upload.Upload', [

            'afiche' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'foto_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
            ],
            'miniatura' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'miniatura_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
            ],
            'portada' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'portada_dir', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
            ],

        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('titulo')
            ->allowEmptyString('titulo');

        $validator
            ->scalar('descripcion')
            ->allowEmptyString('descripcion');

        $validator
            ->scalar('lugar')
            ->allowEmptyString('lugar');

        $validator
            ->scalar('direccion')
            ->allowEmptyString('direccion');

        $validator
            ->scalar('ubicacion')
            ->allowEmptyString('ubicacion');

       
        $validator
            ->scalar('hora')
            ->allowEmptyString('hora');

        $validator
            ->scalar('destacado')
            ->maxLength('destacado', 1)
            ->allowEmptyString('destacado');

        
        $validator
            ->scalar('latitud')
            ->maxLength('latitud', 250)
            ->allowEmptyString('latitud');

        $validator
            ->scalar('longitud')
            ->maxLength('longitud', 250)
            ->allowEmptyString('longitud');

        $validator
            ->scalar('dia_evento')
            ->maxLength('dia_evento', 250)
            ->allowEmptyString('dia_evento');

        $validator
            ->scalar('mes_evento')
            ->maxLength('mes_evento', 250)
            ->allowEmptyString('mes_evento');

        $validator
            ->scalar('año_evento')
            ->maxLength('año_evento', 250)
            ->allowEmptyString('año_evento');

        $validator
            ->scalar('categoria')
            ->allowEmptyString('categoria');

        $validator
            ->scalar('entrada')
            ->allowEmptyString('entrada');

        $validator
            ->scalar('ultimo_dia')
            ->allowEmptyString('ultimo_dia');

        $validator
            ->scalar('ultimo_mes')
            ->allowEmptyString('ultimo_mes');

        $validator
            ->scalar('ultimo_año')
            ->allowEmptyString('ultimo_año');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
