<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Permanente Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $titulo
 * @property string|null $descripcion
 * @property string|null $lugar
 * @property string|null $direccion
 * @property string|null $ubicacion
 * @property string|null $miniatura
 * @property string|null $afiche
 * @property string|null $hora
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property string|null $portada
 * @property string|null $entrada
 * @property string|null $latitud
 * @property string|null $longitud
 * @property string|null $foto_dir
 * @property string|null $miniatura_dir
 * @property string|null $portada_dir
 * @property string|null $dia_evento
 * @property string|null $mes_evento
 * @property string|null $anho_evento
 *
 * @property \App\Model\Entity\User $user
 */
class Permanente extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'titulo' => true,
        'descripcion' => true,
        'lugar' => true,
        'direccion' => true,
        'ubicacion' => true,
        'miniatura' => true,
        'afiche' => true,
        'hora' => true,
        'created' => true,
        'modified' => true,
        'portada' => true,
        'entrada' => true,
        'latitud' => true,
        'longitud' => true,
        'foto_dir' => true,
        'miniatura_dir' => true,
        'portada_dir' => true,
        'dia_evento' => true,
        'mes_evento' => true,
        'anho_evento' => true,
        'vistaprevia' => true,
        'activo' => true,
        'destacar' => true,
        'user' => true
    ];
}
