<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Permanente[]|\Cake\Collection\CollectionInterface $permanentes
 */
?>
<style>
    .buscar, .buscar-inp{
        float:right;
    }
    .buscar-inp{
        margin-top:-30px;
    }
    .buscar{
        margin-top:-30px!important;
    }
</style>
<ol class="breadcrumb">
    <li class="breadcrumb-item">Actividades</li>
    <li class="breadcrumb-item active">Listado</li>
</ol>
<div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <?= $this->Html->link(
                            '<i class="icon-plus"></i>' . __('Agregar').'</a>',
                            ['controller' => 'Permanentes','action' => 'add',],
                            ['escape' => false, 'class' => 'btn btn-success']
                        ) ?>
                         <?= $this->Form->create("",['type'=>'get']) ?>
                                <button class="btn btn-primary buscar">
                                    <i class="fa fa-search"></i>&nbsp;Buscar
                                </button>
                                <div class="col-md-9 buscar-inp">
                                    <?= $this->Form->control('keyword',['default'=>$this->request->query('keyword'),'label' => false,'class'=>'form-control']); ?>
                                </div>
                         <?= $this->Form->end() ?>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                <th>Actividad</th>
                                <th>Lugar</th>
                                <th scope="col" class="actions"><?= __('Acciones') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($permanentes as $permanente): ?>
                                <tr>
                                    <td><?= $this->Number->format($permanente->id) ?></td>
                                    <td><?= h($permanente->titulo) ?></td>
                                    <td><?= h($permanente->lugar) ?></td>
                                    <td class="actions">
                                        
                                        <?= $this->Html->link(__('<i class="icon-note"></i> ' .'Editar'), ['action' => 'edit', $permanente->id],['escape' => false]) ?> &nbsp;&nbsp;
                                        <?php
                                         $permanente = $permanente->id;
                                         echo '<button onclick="eliminar('.$permanente.')" class="btn btn-pill btn-danger" type="button"><i class="fa fa-trash"></i>&nbsp;Eliminar</button>';
                                        ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <nav>
                            <ul class="pagination">
                                <li class="page-item">
                                    <?= $this->Paginator->prev(__('Ant'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item active">
                                <?= $this->Paginator->first( __('Inicio'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->numbers(['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->next(__('Sig'),['class'=>'page-link']) ?>
                                </li>
                                <li class="page-item">
                                <?= $this->Paginator->last(__('Final'),['class'=>'page-link']) ?>
                                </li>
                               
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- Fin ejemplo de tabla Listado -->
</div>
<script>
    function eliminar(name,) {
        Swal({
        title: 'Eliminar!',
        text: "Esta seguro de eliminar esta actividad?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, eliminar!',
        cancelButtonText:'cancelar'
        }).then((result) => {
        if (result.value) {
            location.href="permanentes/delete/"+ name;
        }
        });
    };
</script>