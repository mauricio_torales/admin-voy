<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit User'), ['action' => 'edit', $user->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete User'), ['action' => 'delete', $user->id], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Eventos'), ['controller' => 'Eventos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Evento'), ['controller' => 'Eventos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Permanentes'), ['controller' => 'Permanentes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Permanente'), ['controller' => 'Permanentes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tipo') ?></th>
            <td><?= h($user->tipo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($user->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($user->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Eventos') ?></h4>
        <?php if (!empty($user->eventos)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Titulo') ?></th>
                <th scope="col"><?= __('Descripcion') ?></th>
                <th scope="col"><?= __('Lugar') ?></th>
                <th scope="col"><?= __('Direccion') ?></th>
                <th scope="col"><?= __('Ubicacion') ?></th>
                <th scope="col"><?= __('Miniatura') ?></th>
                <th scope="col"><?= __('Afiche') ?></th>
                <th scope="col"><?= __('Hora') ?></th>
                <th scope="col"><?= __('Destacado') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Portada') ?></th>
                <th scope="col"><?= __('Latitud') ?></th>
                <th scope="col"><?= __('Longitud') ?></th>
                <th scope="col"><?= __('Foto Dir') ?></th>
                <th scope="col"><?= __('Miniatura Dir') ?></th>
                <th scope="col"><?= __('Portada Dir') ?></th>
                <th scope="col"><?= __('Dia Evento') ?></th>
                <th scope="col"><?= __('Mes Evento') ?></th>
                <th scope="col"><?= __('Año Evento') ?></th>
                <th scope="col"><?= __('Categoria') ?></th>
                <th scope="col"><?= __('Entrada') ?></th>
                <th scope="col"><?= __('Ultimo Dia') ?></th>
                <th scope="col"><?= __('Ultimo Mes') ?></th>
                <th scope="col"><?= __('Ultimo Año') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->eventos as $eventos): ?>
            <tr>
                <td><?= h($eventos->id) ?></td>
                <td><?= h($eventos->user_id) ?></td>
                <td><?= h($eventos->titulo) ?></td>
                <td><?= h($eventos->descripcion) ?></td>
                <td><?= h($eventos->lugar) ?></td>
                <td><?= h($eventos->direccion) ?></td>
                <td><?= h($eventos->ubicacion) ?></td>
                <td><?= h($eventos->miniatura) ?></td>
                <td><?= h($eventos->afiche) ?></td>
                <td><?= h($eventos->hora) ?></td>
                <td><?= h($eventos->destacado) ?></td>
                <td><?= h($eventos->created) ?></td>
                <td><?= h($eventos->modified) ?></td>
                <td><?= h($eventos->portada) ?></td>
                <td><?= h($eventos->latitud) ?></td>
                <td><?= h($eventos->longitud) ?></td>
                <td><?= h($eventos->foto_dir) ?></td>
                <td><?= h($eventos->miniatura_dir) ?></td>
                <td><?= h($eventos->portada_dir) ?></td>
                <td><?= h($eventos->dia_evento) ?></td>
                <td><?= h($eventos->mes_evento) ?></td>
                <td><?= h($eventos->año_evento) ?></td>
                <td><?= h($eventos->categoria) ?></td>
                <td><?= h($eventos->entrada) ?></td>
                <td><?= h($eventos->ultimo_dia) ?></td>
                <td><?= h($eventos->ultimo_mes) ?></td>
                <td><?= h($eventos->ultimo_año) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Eventos', 'action' => 'view', $eventos->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Eventos', 'action' => 'edit', $eventos->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Eventos', 'action' => 'delete', $eventos->id], ['confirm' => __('Are you sure you want to delete # {0}?', $eventos->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Permanentes') ?></h4>
        <?php if (!empty($user->permanentes)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Titulo') ?></th>
                <th scope="col"><?= __('Descripcion') ?></th>
                <th scope="col"><?= __('Lugar') ?></th>
                <th scope="col"><?= __('Direccion') ?></th>
                <th scope="col"><?= __('Ubicacion') ?></th>
                <th scope="col"><?= __('Miniatura') ?></th>
                <th scope="col"><?= __('Afiche') ?></th>
                <th scope="col"><?= __('Hora') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Portada') ?></th>
                <th scope="col"><?= __('Entrada') ?></th>
                <th scope="col"><?= __('Latitud') ?></th>
                <th scope="col"><?= __('Longitud') ?></th>
                <th scope="col"><?= __('Foto Dir') ?></th>
                <th scope="col"><?= __('Miniatura Dir') ?></th>
                <th scope="col"><?= __('Portada Dir') ?></th>
                <th scope="col"><?= __('Dia Evento') ?></th>
                <th scope="col"><?= __('Mes Evento') ?></th>
                <th scope="col"><?= __('Anho Evento') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($user->permanentes as $permanentes): ?>
            <tr>
                <td><?= h($permanentes->id) ?></td>
                <td><?= h($permanentes->user_id) ?></td>
                <td><?= h($permanentes->titulo) ?></td>
                <td><?= h($permanentes->descripcion) ?></td>
                <td><?= h($permanentes->lugar) ?></td>
                <td><?= h($permanentes->direccion) ?></td>
                <td><?= h($permanentes->ubicacion) ?></td>
                <td><?= h($permanentes->miniatura) ?></td>
                <td><?= h($permanentes->afiche) ?></td>
                <td><?= h($permanentes->hora) ?></td>
                <td><?= h($permanentes->created) ?></td>
                <td><?= h($permanentes->modified) ?></td>
                <td><?= h($permanentes->portada) ?></td>
                <td><?= h($permanentes->entrada) ?></td>
                <td><?= h($permanentes->latitud) ?></td>
                <td><?= h($permanentes->longitud) ?></td>
                <td><?= h($permanentes->foto_dir) ?></td>
                <td><?= h($permanentes->miniatura_dir) ?></td>
                <td><?= h($permanentes->portada_dir) ?></td>
                <td><?= h($permanentes->dia_evento) ?></td>
                <td><?= h($permanentes->mes_evento) ?></td>
                <td><?= h($permanentes->anho_evento) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Permanentes', 'action' => 'view', $permanentes->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Permanentes', 'action' => 'edit', $permanentes->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Permanentes', 'action' => 'delete', $permanentes->id], ['confirm' => __('Are you sure you want to delete # {0}?', $permanentes->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
