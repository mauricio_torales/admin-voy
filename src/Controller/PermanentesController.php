<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Permanentes Controller
 *
 * @property \App\Model\Table\PermanentesTable $Permanentes
 *
 * @method \App\Model\Entity\Permanente[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PermanentesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $keyword= $this->request->query('keyword');
        if (!empty($keyword)) {
            $this->paginate = [
                'conditions' => ['titulo LIKE'=>'%'.$keyword.'%']
            ];
        }
        $permanentes = $this->paginate($this->Permanentes);

        $this->set(compact('permanentes'));
    }

    /**
     * View method
     *
     * @param string|null $id Permanente id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $permanente = $this->Permanentes->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('permanente', $permanente);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $permanente = $this->Permanentes->newEntity();
        if ($this->request->is('post')) {
            $permanente = $this->Permanentes->patchEntity($permanente, $this->request->getData());
            $permanente->user_id = $this->Auth->user('id');
            if ($this->Permanentes->save($permanente)) {
                $this->Flash->success(__('Actividad creada'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The permanente could not be saved. Please, try again.'));
        }
        $users = $this->Permanentes->Users->find('list', ['limit' => 200]);
        $this->set(compact('permanente', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Permanente id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $permanente = $this->Permanentes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $permanente = $this->Permanentes->patchEntity($permanente, $this->request->getData());
            $permanente->user_id = $this->Auth->user('id');
            if ($this->Permanentes->save($permanente)) {
                $this->Flash->success(__('Se ha actualizado la actividad.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The permanente could not be saved. Please, try again.'));
        }
        $users = $this->Permanentes->Users->find('list', ['limit' => 200]);
        $this->set(compact('permanente', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Permanente id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['get', 'delete']);
        $permanente = $this->Permanentes->get($id);
        if ($this->Permanentes->delete($permanente)) {
            $this->Flash->success(__('Se ha eliminado la actividad.'));
        } else {
            $this->Flash->error(__('The permanente could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
